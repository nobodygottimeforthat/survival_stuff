﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	private static int STARTING_SIZE = 1000;
	// Use this for initialization
	/*
	 *  Strategy used for map generation:
	 *  Generate Planes of ground
	 *  
	 */
	void Start () {

		Vector2 v1 = new Vector2 (Random.Range (-200, 200), Random.Range (-200, 200));
		Vector2 v2 = new Vector2 (Random.Range (-200, 200), Random.Range (-200, 200));
		for (int i = 0; i < 10; i++) {
			
		
			GameObject biome = new GameObject ();


			Vector2 v3 = new Vector2 (Random.Range (-200, 200), Random.Range (-200, 200));
			Vector2 v4 = new Vector2 (Random.Range (-200, 200), Random.Range (-200, 200));
			biome.AddComponent<TestBiome> ();
			biome.GetComponent<TestBiome> ().SetPoints (
				v1, 
				v2,
				new Vector2 (Random.Range (-200, 200), Random.Range (-200, 200)),
				v3, 
				v4
			);
			v1 = v3;
			v2 = v4;

		}
			
	}
	
	// Update is called once per frame
	void Update () {
		//biome.GetComponent<TestBiome> ().mat.SetTexture("Texture", Resources.Load<Texture> ("Grass.png"));
	}
}

