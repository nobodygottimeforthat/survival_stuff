﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TerrainMaker : MonoBehaviour {

    public int sizeX = 100;
    public int sizeY = 100;
    public GameObject dirtBlock;
    public GameObject waterBlock;
    public GameObject player;

	// Use this for initialization
	void Start () {
        int[,] bitMap = new int[sizeX, sizeY];

        CreateBioms(bitMap);
        FillWithWater(bitMap);
        PlacePlayer(bitMap);
    }
    
    // Creates the bioms.(Forests, Snow, Grass, Dessert)
    void CreateBioms(int[,] bitMap)
    {
        for (int x = 10; x < 50; x++)
        {
            for (int y = 10; y < 50; y++)
            {
                dirtBlock.transform.position = new Vector2(x, y);
                Instantiate(dirtBlock);
                bitMap[x, y] = 1;
            }
        }
    }

    // Fills the non-block area with water.
    void FillWithWater(int[,] bitMap)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (bitMap[x,y] == 1)
                {
                    continue;
                }
                else
                {
                    waterBlock.transform.position = new Vector2(x, y);
                    Instantiate(waterBlock);
                }
            }
        }
    }

    // Create grass biom.
    void CreateGrassBiom()
    {

    }

    // Create desert biom.
    void CreateDesertBiom()
    {

    }

    // Create forest biom.
    void CreateForestBiom()
    {

    }

    // Create snow biom.
    void CreateSnowBiom()
    {

    }

    // Creates the loot sites.
    void CreateSites()
    {

    }

    // Creates the ore deposits around the map.
    void CreateDeposits()
    {

    }

    // Places the player on a random location on the map.
    void PlacePlayer(int[,] bitMap)
    {
        int x, y;
        while (true)
        {
            x = Random.Range(1, sizeX);
            y = Random.Range(1, sizeY);

            // Checks if location is placeable (not water).
            if (bitMap[x,y] == 1)
            {
                break;
            }
        }
        player.transform.position = new Vector2(x, y);
    }
}

