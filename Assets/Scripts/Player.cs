﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    // Stats vars:
    public float health = 100;
    public float hunger = 100;
    public float thirst = 100;

    // Stats GUI:
    public GameObject healthBar;
    public GameObject hungerBar;
    public GameObject thirstBar;

    // Movment vars:
    public float speed = 30;

    // Shooting:
    

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        Decay();
        Shoot();
	}

    private void FixedUpdate()
    {
        MovePlayer();
    }

    // Moves the player horizontally and vertically.
    private void MovePlayer()
    {
        var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Quaternion rot = Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward);
        float vertPress = Input.GetAxisRaw("Vertical");
        float horzPress = Input.GetAxisRaw("Horizontal");

        // Look to mouse pos.
        transform.rotation = rot;
        transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
        GetComponent<Rigidbody2D>().angularVelocity = 0;

        // Move.
        GetComponent<Rigidbody2D>().velocity = new Vector2(horzPress, vertPress) * speed;
    }

    // Not implemented yet!
    private void Shoot()
    {
        if (Input.GetMouseButton(0))
        {
            
        }
    }

    // Overtime hunger and thirst are decreased.
    private void Decay()
    {
        hunger -= (float)0.001;
        thirst -= (float)0.01;
    }

    // Used to update the GUI. (Can be changed to by referance to avoid the func calls???)
    private void UpdateGUI()
    {
    }
}
